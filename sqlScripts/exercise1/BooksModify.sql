USE bookstore;
ALTER TABLE BOOKS ADD COLUMN publishing_house VARCHAR(50);
ALTER TABLE BOOKS ADD COLUMN binding ENUM("без переплёта", "твёрдый", "мягкий");
UPDATE BOOKS SET binding = 1;
UPDATE BOOKS SET publishing_house = "ОмскПечать" WHERE id BETWEEN 40 and 85;
ALTER TABLE BOOKS RENAME COLUMN title to bookname;
ALTER TABLE BOOKS RENAME BOOOOOOKS;
DROP TABLE BOOOOOOKS;