DROP DATABASE IF EXISTS `mydb`;
CREATE DATABASE `mydb`;

USE `mydb`;

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `organization_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_departments_organization1_idx` (`organization_id`),
  CONSTRAINT `department_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_employee_department_idx` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `employee_id` int NOT NULL,
  `timestart` time NOT NULL,
  `timeend` time NOT NULL,
  `weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedule_employee1_idx` (`employee_id`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `schedule_chk_1` CHECK ((`timeend` > `timestart`)),
  CONSTRAINT `schedule_chk_2` CHECK (((`timeend` - interval 4 hour) > `timestart`)),
  CONSTRAINT `schedule_chk_3` CHECK ((`timestart` > _utf8mb3'06:00:00')),
  CONSTRAINT `schedule_chk_4` CHECK ((`timeend` <= _utf8mb3'23:59:59'))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;