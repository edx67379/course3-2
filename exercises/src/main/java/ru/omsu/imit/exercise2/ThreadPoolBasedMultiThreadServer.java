package ru.omsu.imit.exercise2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolBasedMultiThreadServer {
	static int port = 6666;
	static final Logger LOGGER = LoggerFactory.getLogger(ThreadPoolBasedMultiThreadServer.class);

	public static void main(String[] args) throws Exception {
		ExecutorService executorService = Executors.newCachedThreadPool();
		LOGGER.debug("Server started and ready to accept client requests");
		try (ServerSocket serverSocket = new ServerSocket(port)) {
			int id = 0;
			while (true) {
				Socket clientSocket = serverSocket.accept();
				executorService.execute(new ClientServiceThread(clientSocket, id++, (int) (Math.random() * 10000 + 1)));
			}
		}
	}
}