package ru.omsu.imit.exercise2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientServiceThread extends Thread {
	private Socket clientSocket;
	private int clientID;
	private int number;
	public ClientServiceThread(Socket socket, int id, int number) {
		clientSocket = socket;
		clientID = id;
		this.number = number;
	}

	public void run() {
		ThreadPoolBasedMultiThreadServer.LOGGER.debug("Accepted Client : ID - " +
				clientID + " : Address - " + clientSocket.getInetAddress().getHostName());
		try (DataInputStream in = new DataInputStream(clientSocket.getInputStream());
				DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream())) {
			int attempt = 0;
			while (true) {
				int clientCommand = in.readInt();
				attempt++;
				if (clientCommand == number) {
					ThreadPoolBasedMultiThreadServer.LOGGER.debug("Stopping client thread for client: " + clientID +
							" attempt = " + attempt);
					out.writeUTF("Nice!");
					break;
				} else if(clientCommand < number){
					out.writeUTF("Few!");
					out.flush();
				} else {
					out.writeUTF("Lot!");
					out.flush();
				}
			}
		} catch (IOException e) {
			ThreadPoolBasedMultiThreadServer.LOGGER.info(e.getMessage());
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				ThreadPoolBasedMultiThreadServer.LOGGER.info(e.getMessage());
			}
		}

	}
}