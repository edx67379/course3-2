package ru.omsu.imit.exercise2;

import java.net.*;
import java.io.*;

public class Client {
	public static void main(String[] ar) {
		final int serverPort = 6666;
		final String address = "localhost";
		InetAddress ipAddress;
		try {
			ipAddress = InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return;
		}
		try (Socket socket = new Socket(ipAddress, serverPort);
				DataInputStream in = new DataInputStream(socket.getInputStream());
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {
			String line = null;
			System.out.println("Guess the number from 1 to 10000");

			while (true) {
				int number = Integer.parseInt(keyboardReader.readLine());
				out.writeInt(number);
				out.flush();
				line = in.readUTF();
				System.out.println("Server answer is : " + line);
				if(line.equalsIgnoreCase("Nice!")) {
					System.out.println("Client stopped");
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
}