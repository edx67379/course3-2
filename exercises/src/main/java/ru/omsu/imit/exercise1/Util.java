package ru.omsu.imit.exercise1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class Util {

    private static final String URL = "jdbc:mysql://localhost:3306/bookstore";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String args[]) {

        if (!loadDriver())
            return;

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            List<Book> books = Service.getAllBooks(con);
            for(Book b : books) {
                System.out.println(b);
            }

            Service.updateBooksPagesWherePagesMoreStartWithWord(con, 0, 500, "Книга");

            System.out.println();
            books = Service.getAllBooks(con);
            for(Book b : books) {
                System.out.println(b);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
}
