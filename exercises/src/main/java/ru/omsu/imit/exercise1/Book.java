package ru.omsu.imit.exercise1;

public class Book {
    private int id;
    private String title;
    private int year;
    private int pages;
    private String publishingHouse;
    private String binding;

    public Book(int id, String title, int year, int pages, String publishingHouse, String binding) {
        super();
        this.id = id;
        this.title = title;
        this.year = year;
        this.pages = pages;
        this.publishingHouse = publishingHouse;
        this.binding = binding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishing_house(String publishing_house) {
        this.publishingHouse = publishing_house;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    @Override
    public String toString() {
        return "ru.omsu.imit.exercise1.Book [id=" + id + ", title=" + title + ", year=" + year + ", pages=" + pages +
                ", publishing_house=" + publishingHouse + ", binding=" + binding + "]";
    }
}

