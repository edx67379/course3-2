package ru.omsu.imit.exercise1;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Service {
    public static void insertBook(Connection con, Book book) {
        String query = "INSERT INTO BOOKS VALUES(?, ?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, book.getTitle());
            stmt.setInt(3, book.getYear());
            stmt.setInt(4, book.getPages());
            stmt.setString(5, book.getPublishingHouse());
            stmt.setString(6, book.getBinding());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Book> getAllBooks(Connection con) {
        String query = "SELECT * FROM BOOKS";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                Book book = new Book(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return books;
    }

    public static List<Book> getNFirstBooks(Connection con, int limit) {
        String query = "SELECT * FROM BOOKS LIMIT ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, limit);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksById(Connection con, int id) {
        String query = "SELECT * FROM BOOKS WHERE id = ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksWithSpecifiedTitle(Connection con, String title) {
        String query = "SELECT * FROM BOOKS WHERE title = ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, title);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksStartWithWord(Connection con, String word) {
        String query = "SELECT * FROM BOOKS WHERE title LIKE ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, word + "%");
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksEndWithWord(Connection con, String word) {
        String query = "SELECT * FROM BOOKS WHERE title LIKE ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, "%" + word);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksFromYearToYear(Connection con, int year1, int year2) {
        String query = "SELECT * FROM BOOKS WHERE year BETWEEN ? and ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, year1);
            stmt.setInt(2, year2);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksFromPagesToPages(Connection con, int pages1, int pages2) {
        String query = "SELECT * FROM BOOKS WHERE pages BETWEEN ? and ?";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages1);
            stmt.setInt(2, pages2);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static List<Book> getBooksFromPagesToPagesFromYearToYear(Connection con, int pages1, int pages2, int year1, int year2) {
        String query = "SELECT * FROM BOOKS WHERE (pages BETWEEN ? and ?) and (year BETWEEN ? and ?)";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages1);
            stmt.setInt(2, pages2);
            stmt.setInt(3, year1);
            stmt.setInt(4, year2);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Book book = new Book(rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getString(5),
                            rs.getString(6));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static void updateAllBooksPages(Connection con, int pages) {
        String query = "UPDATE BOOKS SET pages = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateBooksPagesWherePagesMore(Connection con, int pages1, int pages2) {
        String query = "UPDATE BOOKS SET pages = ? WHERE pages > ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages1);
            stmt.setInt(2, pages2);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateBooksPagesWherePagesMoreYearLess(Connection con, int pages1, int pages2, int year) {
        String query = "UPDATE BOOKS SET pages = ? WHERE pages > ? and year < ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages1);
            stmt.setInt(2, pages2);
            stmt.setInt(3, year);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateBooksPagesWherePagesMoreStartWithWord(Connection con, int pages1, int pages2, String word) {
        String query = "UPDATE BOOKS SET pages = ? WHERE pages > ? and title LIKE ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages1);
            stmt.setInt(2, pages2);
            stmt.setString(3, word + "%");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksWherePagesMore(Connection con, int pages) {
        String query = "DELETE FROM BOOKS WHERE pages > ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, pages);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBookStartWithWord(Connection con, String word) {
        String query = "DELETE FROM BOOKS WHERE title LIKE ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, word + "%");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBookFromIdToId(Connection con, int id1, int id2) {
        String query = "DELETE FROM BOOKS WHERE id BETWEEN ? and ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, id1);
            stmt.setInt(2, id2);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBookWithSpecifiedTitle(Connection con, String title) {
        String query = "DELETE FROM BOOKS WHERE title = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, title);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBookWithSpecifiedTitles(Connection con, String title1, String title2, String title3) {
        String query = "DELETE FROM BOOKS WHERE title IN (?, ?, ?)";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, title1);
            stmt.setString(2, title2);
            stmt.setString(3, title3);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAllBooks(Connection con) {
        String query = "DELETE FROM BOOKS";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateAllBooksBinding(Connection con) {
        String query = "UPDATE BOOKS SET binding = \"без переплёта\"";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateBooksPublishingHouseFromIdToId(Connection con, String publishingHouse, int id1, int id2) {
        String query = "UPDATE BOOKS SET publishing_house = ? WHERE id BETWEEN ? and ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, publishingHouse);
            stmt.setInt(2, id1);
            stmt.setInt(3, id2);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void changeColumnNameTitleToBookname(Connection con) {
        String query = "ALTER TABLE BOOKS RENAME COLUMN title to bookname";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void changeTableName(Connection con, String name) {
        String query = "ALTER TABLE BOOKS RENAME " + name;
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropTable(Connection con, String name) {
        String query = "DROP TABLE " + name;
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
